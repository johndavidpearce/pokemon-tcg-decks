
function _set(set) {
	set.releaseDate = new Date(set.releaseDate);
	set.updatedAt = new Date(set.updatedAt);
	return set;
}

async function getSets() {
	return (await (await fetch('/sets')).json()).map(_set);
}

async function getSet(setId) {
	return _set(await (await fetch(`/sets/${setId}`)).json());
}

function _zero(num) {
	return `${num < 10 ? '0' : ''}${num}`;
}

function dateUTCStr(date, format = 'short') {
	const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
	const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dev'];
	if (!date) { return; }
	switch (format) {
		case 'short':
			return `${_zero(date.getUTCMonth() + 1)}/${date.getUTCFullYear()}`;
		case 'full':
			return `${days[date.getUTCDay()]}, ${date.getUTCDate()} ${months[date.getUTCMonth()]} ${date.getUTCFullYear()} ${_zero(date.getUTCHours())}:${_zero(date.getUTCMinutes())}:${_zero(date.getUTCSeconds())}`;
		case 'medium':
		default:
			return `${date.getUTCDate()} ${months[date.getUTCMonth()]} ${date.getUTCFullYear()}`;
	}
}

function toClassName(text) {
	return (text || '')
		.toLowerCase()
		.replace('é', 'e')
		.replace(' ', '-');
}

function cardTypeHtml(type) {
	return `<span class="energy-type type-${toClassName(type)}" title="${type}"></span>`
}

function cardHtml(card, classes = '') {
	const countAttr = card.count ? `data-count="${card.count}"` : '';
	return `
		<a href="cards.html?card=${card.id}" class="poke-card ${classes || ''}" title="${card.name}" ${countAttr} data-number="${card.number}">
			<img alt="${card.name} low res" src="${card.imageUrl}" class="low-res" width="245" height="342"/>
			<img alt="${card.name} high res" src="${card.imageUrlHiRes}" class="high-res"/>
			<span class="card-set">
				<span class="set-title">#${card.number} - ${card.series}</span><br/>
				<img alt="${card.set} set icon" title="${card.series}: ${card.set} #${card.number}" src="https://images.pokemontcg.io/${card.setCode}/logo.png"/>
			</span>
		</a>`;
}

function setHtml(set, card = null) {
	// {"code":"base1","ptcgoCode":"BS","name":"Base","series":"Base","totalCards":102,
	// "standardLegal":false,"expandedLegal":false,"releaseDate":"01/09/1999",
	// "symbolUrl":"https://images.pokemontcg.io/base1/symbol.png","logoUrl":"https://images.pokemontcg.io/base1/logo.png",
	// "updatedAt":"08/14/2020 09:35:00"}
	const cards = card ? `(${card.number} / ${set.totalCards})` : `x${set.totalCards}`;
	return `
		<img class="set-symbol" width="24" src="${set.symbolUrl}"/>
		${set.series}: <a href="set.html?set=${set.code}" target="_blank">${set.name}</a>
		${cards}
		<img class="set-logo" height="24" src="${set.logoUrl}"/>
		<span class="set-released" title="${dateUTCStr(set.releaseDate, 'full')}">${dateUTCStr(set.releaseDate)}</span>
		<span class="set-updated" title="${dateUTCStr(set.updatedAt, 'full')}">${dateUTCStr(set.updatedAt)}</span>
		<span class="set-legal">
			<span class="standard" title="Standard Legal">${set.standardLegal ? '✅' : '❌'}</span>
			<span class="expanded" title="Expanded Legal">${set.expandedLegal ? '✅' : '❌'}</span>
		</span>
	`;
}

function cardStatsHtml(stats) {
	function n(num) {
		const result = Math.round(num * 100) / 100;
		try {
			return new Intl.NumberFormat().format(result);
		} catch {
			return result;
		}
	}
	function breakdown(stat, keyHtml = k => `${k}: `) {
		return Object.keys(stat).map(k => `${keyHtml(k)}<b>${stat[k]}</b>`).join(', ');
	}
	function bool(stat) {
		return `${stat.true || 0} <span class="of">of ${(stat.true || 0) + (stat.false || 0)}</span>`;
	}
	function numeric(stat) {
		return `<b>μ</b> ${n(stat.mean)} <b>σ</b> ${n(stat.stddev)} <b>Σ</b> ${n(stat.sum)} <b>N</b> ${n(stat.count)}`;
	}
	function range(stat) {
		return `${stat.min}..${stat.max}`;
	}
	return `<dl class="stats">
		<dt>HP</dt><dd>${numeric(stats.hp)}</dd>
		<dt>Attack damage</dt><dd>${numeric(stats.attackDamage)}</dd>
		<dt>Attack cost</dt><dd>${numeric(stats.attackCost)}</dd>
		<dt>Retreat cost</dt><dd>${numeric(stats.convertedRetreatCost)}</dd>
		<dt>Supertype</dt><dd>${breakdown(stats.supertype)}</dd>
		<dt>Subtype</dt><dd>${breakdown(stats.subtype)}</dd>
		<dt>Ancient traits</dt><dd>${bool(stats.ancientTrait)}</dd>
		<dt>Abilities</dt><dd>${bool(stats.ability)}</dd>
		<dt>Weaknesses</dt><dd>${bool(stats.weaknesses)}</dd>
		<dt>Resistances</dt><dd>${bool(stats.resistances)}</dd>
		<dt>Pokedex</dt><dd>${range(stats.nationalPokedexNumber)}</dd>
		<dt>Rarity</dt><dd>${breakdown(stats.rarity)}</dd>
		<dt>Types</dt><dd>${breakdown(stats.types, t => cardTypeHtml(t) + 'x')}</dd>
		<dt>Series</dt><dd>${breakdown(stats.series)}</dd>
		<dt>Set</dt><dd>${breakdown(stats.set)}</dd>
	</dl>`;
}

const TypeIcon = {
	Fire: '🔥',
	Fighting: '👊',
	Colorless: '✵',
	Water: '💧',
	Grass: '🌱',
	Lightning: '⚡',
	Psychic: '👁',
	Metal: '🔪',
	Darkness: '🌑',
	Fairy: '🧚‍️',
	Dragon: '🐲'
};
