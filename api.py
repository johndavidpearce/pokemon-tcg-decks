#!/usr/bin/env python3
import json
import math
import os
import sys
import re
from collections import OrderedDict, defaultdict
from datetime import datetime

from flask import Flask, request, jsonify, send_file, send_from_directory, request
from flask.json import JSONEncoder as JSONEncoderBase


class JSONEncoder(JSONEncoderBase):
    def default(self, o):
        if isinstance(o, (tuple, set)):
            return list(o)
        return super().default(o)


def minmax(iterable):
    min_x = None
    max_x = None
    for x in iterable:
        if min_x is None or x < min_x:
            min_x = x
        if max_x is None or x > max_x:
            max_x = x
    return min_x, max_x


app = Flask('pokemon-tcg-decks', static_url_path='')
app.json_encoder = JSONEncoder
data = 'pokemon-tcg-data/json'

_card_cache = OrderedDict()
_card_names = defaultdict(list)
_deck_lookup = OrderedDict()
_card_sets = OrderedDict()
_cards_by_set = defaultdict(list)
_card_types = defaultdict(list)
_card_subtypes = defaultdict(list)
_card_supertypes = defaultdict(list)
_card_series = defaultdict(list)
_card_artist = defaultdict(list)


def card_lookup():
    if not _card_cache:
        for path, dirs, files in os.walk(os.path.join(data, 'cards')):
            del dirs[:]
            for name in files:
                with open(os.path.join(path, name), 'rt') as f:
                    cards = json.load(f)
                    for card in cards:
                        _card_cache[card['id']] = card
                        _card_names[card['name']].append(card)
                        for card_type in card.get('types', []):
                            _card_types[card_type].append(card)
                        _card_subtypes[card.get('subtype') or '(none)'].append(card)
                        _card_supertypes[card.get('supertype') or '(none)'].append(card)
                        _cards_by_set[card['setCode']].append(card)
                        _card_series[card.get('series') or '(none)'].append(card)
                        _card_artist[card.get('artist') or '(none)'].append(card)
        print(f'Loaded {len(_card_cache):,} cards')
    return _card_cache


def deck_lookup():
    if not _deck_lookup:
        cards = card_lookup()
        for path, dirs, files in os.walk(os.path.join(data, 'decks')):
            for name in files:
                with open(os.path.join(path, name), 'rt') as f:
                    decks = json.load(f)
                    for deck in decks:
                        # some decks don't have IDs; generate one
                        deck['id'] = deck.get('id') or f"noid--{re.sub(r'[^a-zA-Z0-9]', '-', deck['name'])}"
                        # collect the card sets that the deck is made up of (determines deck era)
                        deck['sets'] = {
                            cards[c['id']]['setCode']
                            for c in deck['cards']
                            # ignore basic energies, as they can come from any set
                            if not (cards[c['id']]['subtype'] == 'Basic' and cards[c['id']]['supertype'] == 'Energy')
                        }
                        _deck_lookup[deck['id']] = deck
        print(f'Loaded {len(_deck_lookup)} decks')
    return _deck_lookup


def set_lookup():
    if not _card_sets:
        with open('data/sets.json', 'rt') as f:
            sets = json.load(f)
            for card_set in sets['sets']:
                card_set['releaseDate'] = datetime.strptime(card_set['releaseDate'], '%m/%d/%Y')
                card_set['updatedAt'] = datetime.strptime(card_set['updatedAt'], '%m/%d/%Y %H:%M:%S')
                _card_sets[card_set['code']] = card_set
        print(f'Loaded {len(_card_sets)} sets')
    return _card_sets


class Accumulation:
    """
    Accumulates values, counting distinct values and calculates statistics on them.
    """

    def __init__(self):
        self._values = defaultdict(int)
        self._count = 0
        self._sum = 0
        self._min = None
        self._max = None

    def append(self, value, count=1):
        if value is None or value == '':
            pass
        elif isinstance(value, (list, set, tuple)):
            self.extend(value, count=count)
        elif isinstance(value, str):
            self._values[value] += count
        elif isinstance(value, bool):
            self._values[str(value).lower()] += count
        elif isinstance(value, (float, int)):
            self._values[value] += count
            self._sum += value * count
            self._count += count
            if self._min is None or value < self._min:
                self._min = value
            if self._max is None or value > self._max:
                self._max = value
        else:
            raise ValueError(f'Non-accumulation type {type(value).__name__} - {value}')

    def extend(self, iterable, count=1):
        for v in iterable:
            self.append(v, count=count)

    def __iter__(self):
        for x, repeat in self._values.items():
            for _ in range(repeat):
                yield x

    def __len__(self):
        return self._count or sum(self._values.values())

    @property
    def is_numeric(self):
        return self._count

    def stats(self):
        if self.is_numeric:
            items = sorted(self._values.items(), key=lambda item: item[1])
            mean = self._sum / self._count
            return {
                'mean': mean,
                'mode': items[-1][0],
                'median': items[len(items) // 2][0],
                'stddev': math.sqrt(
                    sum((x - mean) ** 2 * repeat for x, repeat in self._values.items())
                    / self._count
                ),
                'sum': self._sum,
                'min': self._min,
                'max': self._max,
                'count': self._count,
            }
        return self._values


def card_stats(cards):
    """
    Given a list of cards, calculate statistical information for them, such as average hit points,
    attack damage, etc.
    """
    accumulations = defaultdict(Accumulation)
    for card in cards:
        count = card.get('count') or 1
        for prop in ('supertype',):
            accumulations[prop].append(card.get(prop), count=count)
        # ignore Basic Energies for everything except supertype
        if card.get('supertype') == 'Energy' and card.get('subtype') == 'Basic':
            continue
        # numeric & string sets
        for prop in ('count', 'setCode', 'series', 'nationalPokedexNumber', 'level', 'types', 'subtype', 'convertedRetreatCost', 'set', 'rarity'):
            accumulations[prop].append(card.get(prop), count=count)
        # str -> number (hp)
        for prop in ('hp',):
            value = card.get(prop)
            if value:
                accumulations[prop].append(int(value), count=count)
        # bools
        for prop in ('ancientTrait', 'ability', 'evolvesFrom', 'weaknesses', 'resistances'):
            accumulations[prop].append(bool(card.get(prop)), count=count)
        # lists
        if card.get('evolvesTo'):
            accumulations['evolvesTo'].append(len(card['evolvesTo']), count=count)
        # text: 'text', 'flavorText',
        # other: 'attacks'
        for i, attack in enumerate(card.get('attacks', [])):
            for key_start in ('attack', f'attack{i + 1}'):
                if attack.get('damage'):
                    accumulations[key_start + 'Damage'].append(
                        int(attack['damage'].strip('x+×-')), count=count)
                accumulations[key_start + 'Cost'].append(attack['convertedEnergyCost'], count=count)
    return {
        k: accum.stats() for k, accum in accumulations.items()
    }


@app.route('/', methods=['GET'])
def home():
    return send_file('web/index.html')


@app.route('/decks', methods=['GET'])
def get_decks():
    sets = set_lookup()
    return jsonify(sorted((
        {
            **{k: deck[k] for k in ('id', 'name', 'types', 'sets')},
            'dateRange': minmax(sets[s]['releaseDate'] for s in deck['sets']),
            'standardLegal': all(sets[s]['standardLegal'] for s in deck['sets']),
            'expandedLegal': all(sets[s]['expandedLegal'] for s in deck['sets'])
        }
        for deck in deck_lookup().values()
    ), key=lambda d: d['dateRange'], reverse=True))


@app.route('/deck/<string:deck_id>', methods=['GET'])
def get_deck(deck_id):
    sets = set_lookup()
    try:
        cards = card_lookup()
        deck = deck_lookup()[deck_id]
        deck_cards = [
            {
                **card,
                **cards[card['id']]
            }
            for card in deck['cards']
        ]
        return jsonify({
            **deck,
            # decorate the card information with the full card info
            'cards': deck_cards,
            # include full set info
            'sets': sorted((sets[s] for s in deck['sets']), key=lambda s: s['releaseDate']),
            'stats': card_stats(deck_cards)
        })
    except KeyError:
        return 'Not Found', 404


@app.route('/card/<string:card_id>', methods=['GET'])
def get_card(card_id):
    try:
        return jsonify(card_lookup()[card_id])
    except KeyError:
        return 'Not Found', 404


@app.route('/cards', methods=['GET'])
def get_cards():
    name = request.args.get('name')
    card_lookup()  # generate cache
    try:
        return jsonify(_card_names[name])
    except KeyError:
        return 'Not Found', 404


@app.route('/cards/names', methods=['GET'])
def get_card_names():
    card_lookup()  # generate cache
    try:
        return jsonify(sorted(_card_names))
    except KeyError:
        return 'Not Found', 404


@app.route('/sets', methods=['GET'])
def get_sets():
    return jsonify(list(reversed(set_lookup().values())))


@app.route('/sets/<string:set_id>', methods=['GET'])
def get_set(set_id):
    card_lookup()  # generate cache
    try:
        return jsonify({
            **set_lookup()[set_id],
            'cards': _cards_by_set[set_id],
            'stats': card_stats(_cards_by_set[set_id]),
        })
    except KeyError:
        return 'Not Found', 404


@app.route('/types', methods=['GET'])
def get_types():
    card_lookup()  # generate cache
    return jsonify(sorted(_card_types))


@app.route('/subtypes', methods=['GET'])
def get_subtypes():
    card_lookup()  # generate cache
    return jsonify(sorted(_card_subtypes))


@app.route('/supertypes', methods=['GET'])
def get_supertypes():
    card_lookup()  # generate cache
    return jsonify(sorted(_card_supertypes))


@app.route('/series', methods=['GET'])
def get_series():
    card_lookup()  # generate cache
    return jsonify(sorted(_card_series))


@app.route('/artists', methods=['GET'])
def get_artists():
    card_lookup()  # generate cache
    return jsonify(sorted(_card_artist))


@app.route('/card/<string:card_id>/evolutions', methods=['GET'])
def get_card_evolutions(card_id):
    def card_by_name(name, ref_card):  # find a card by name
        cards = _card_names[name]
        try:
            # prefer cards in the same set, where possible
            return [c for c in cards if c.get('setCode') == ref_card.get('setCode')][0]
        except LookupError:
            return cards[0]

    def base_of(card):  # previous evolution
        return card_by_name(card.get('evolvesFrom'), card)

    def basic_of(card):  # earliest possible evolution (i.e. Basic)
        base = card
        while True:
            try:
                base = base_of(base)
            except LookupError:
                return base

    def final_evolutions(card):  # final evolution(s)
        if not card.get('evolvesTo'):
            return card
        for name in card['evolvesTo']:
            for final in final_evolutions(card_by_name(name, card)):
                yield final

    def next_of(card):  # next evolutions (direct)
        for name in card.get('evolvesTo', []):
            yield card_by_name(name, card)

    def evolution_tree(card):
        return {
            'card': card,
            'evolutions': [evolution_tree(n) for n in next_of(card)]
        }

    # starting at this card, get the list of cards in each subsequent evolution stage.
    # this collects all future evolutions, grouped by stage.
    def evolution_stages(card):
        def _next_stage(cards):  # cards <= multiple cards of the same stage
            stage = OrderedDict()
            for stage_card in cards:
                for next_stage_card in next_of(stage_card):
                    stage[next_stage_card['name']] = next_stage_card
            return stage.values()

        next_stage = [card]
        while next_stage:
            yield next_stage
            next_stage = list(_next_stage(next_stage))

    try:
        start_card = card_lookup()[card_id]
    except KeyError:
        return 'Not Found', 404
    # find the earliest evolution, and generate the evolution tree
    basic = basic_of(start_card)
    sets = set_lookup()
    return jsonify({
        'card': start_card,
        'set': sets[start_card['setCode']],
        'stages': list(evolution_stages(basic)),
        'tree': evolution_tree(basic),
        'versions': sorted(
            _card_names[start_card['name']],
            key=lambda c: sets[c['setCode']]['releaseDate'],
            reverse=True)
    })


@app.route('/web/<path:path>', methods=['HEAD', 'GET', 'OPTIONS'])
def static_files(path):
    return send_from_directory('web', path)


def main():
    try:
        # startup Flask
        app.run(port=6001)
    except KeyboardInterrupt:
        print('Bye')


if __name__ == '__main__':
    sys.exit(main())
