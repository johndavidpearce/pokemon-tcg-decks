# Pokemon TCG: Deck Viewer #

Pokemon The Card Game - Deck Viewer.

Aims of this project:

* view pre-built decks, helping newbies (like me) learn about deck building
* view card sets (collections of cards released at particular points in time, which are typically
  sold together), helping newbies understand what sets to purchase from, and how cards from other
  sets will differ (e.g. new cards vs. old ones).

## Project Setup ##

Requires Python and a web browser.
Download the _Pokemon TCG data_ before starting.

```
cd pokemon-tcg-decks
git clone https://github.com/PokemonTCG/pokemon-tcg-data
pip install -r requirements.txt
python api.py
open http://localhost:5000
```

## With Thanks ##

This project is based on the data kindly provided by
[Pokémon TCG API](https://pokemontcg.io/).
This excellent project includes a user interface for viewing card information as well as search
functionality, and is highly recommended.

## Apologies ##

This is a hurriedly hacked-together project, please excuse the many shortcuts taken.
Please don't judge the usual quality of my work based on this project 😉.

## Background ##

I am a newbie Pokemon Card Game player. I collected as a kid, but never player. Recently my 7 year
old has become very interested in collecting, and I didn't want her to miss out on playing the
actual game (and the understanding of the value of the cards) like I did. I dug up my old cards and
was disappointed to see that their power was far outmatched by the newer cards. I started this
project to understand how we could build a deck from what we had, and how I could make use of my old
cards in light of the new more powerful ones.
